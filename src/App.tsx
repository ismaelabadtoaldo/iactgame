import { Header } from "./components/Header";
import { Dashboard } from "./components/Dashboard";
import { GlobalStyle } from "./styles/global";
import { ActionsContext } from "./ActionsContext";

export function App() {
  return (
    <ActionsContext.Provider value={[]}>
      <Header />
      <Dashboard />
      <GlobalStyle />
    </ActionsContext.Provider>
  );
}
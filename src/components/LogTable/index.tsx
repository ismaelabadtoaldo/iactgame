import { Container } from "./style";
import { Table } from "react-bootstrap";


export function LogTable() {
    return(
        <Container>
            <div>
                <Table>
                    <thead>
                    <tr>
                        <th>Log da batalha</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td className="give-up">Você desistiu :(</td>
                    </tr>
                    <tr>
                        <td className="heal">Você curou (+10) do seu HP</td>
                    </tr>
                    <tr>
                        <td className="damage">Monstro tirou (-11) do seu HP</td>
                    </tr>
                    <tr>
                        <td className="attack">Você tirou (-11) do Monstro</td>
                    </tr>
                    <tr>
                        <td className="damage">Monstro tirou (-11) do seu HP</td>
                    </tr>
                    <tr>
                        <td className="attack">Você tirou (-11) do Monstro</td>
                    </tr>
                    <tr>
                        <td className="damage">Monstro tirou (-11) do seu HP</td>
                    </tr>
                    <tr>
                        <td className="attack">Você tirou (-11) do Monstro</td>
                    </tr>
                    </tbody>
                </Table>
            </div>
        </Container>
    );
}
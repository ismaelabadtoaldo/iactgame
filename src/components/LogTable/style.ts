import styled from "styled-components";

export const Container = styled.div`
    Table {
        border-collapse: separate;
        border-spacing: 0 0.5rem;

        th {
            font-weight: 600;
            font-size: 1.2rem;
            color: var(--text-title);
            padding: 1rem 0;
            text-align: center;
        }

        td {
            padding: 1rem 0;
            border: 0;
            border-radius: 0.25rem;
            text-align: center;
            color: var(--white);
            font-weight: 600;

            &.damage {
                background-color: var(--orange);
            }

            &.attack {
                background-color: var(--blue);
            }

            &.heal {
                background-color: var(--green)
            }

            &.give-up {
                background-color: var(--red)
            }
        }
    }
`;
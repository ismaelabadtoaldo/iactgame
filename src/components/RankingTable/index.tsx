import { useEffect } from "react";
import { Container } from "./style"; 
import { api } from "../../services/api";

export function RankingTable() {
    useEffect(() => {
        api.get('/ranking').then(response => console.log(response.data))
    }, []);

    return(
        <Container>
        </Container>
    );
}
import { Action } from "../Action";
import { LogTable } from "../LogTable";
import { Container } from "./styles";

export function Dashboard() {
    return (
        <Container>
            <Action />
            <LogTable />
        </Container>
    );
}
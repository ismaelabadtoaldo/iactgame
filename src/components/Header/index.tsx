import { Container, Content } from "./styles";

export function Header() {
    return (
        <Container>
            <Content>
                <div>Home</div>
                <div>Ranking</div>
            </Content>
        </Container>
    )
}
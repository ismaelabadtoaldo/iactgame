import styled from "styled-components";

export const Container = styled.header`
    background: var(--blue);
`;

export const Content = styled.div`
    max-width: 1120px;
    margin: 0 auto;
    padding: 1rem 0 3rem;
    display: flex;
    align-items: center;
    justify-content: space-between;

    div {
        font-weight: 600;
        color: white;
    }
`;

import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { HealthBar } from "../HealthBar";
import { Container, ContainerButtons } from "./styles";


export function Action() {
    const [playerTurn, setPlayerTurn] = useState(true);

    const [playerHP, setPlayerHP] = useState(100);
    const [monsterHP, setMonsterHP] = useState(100);

    const [monsterAtk, setMonsterAtk] = useState(getRndInteger(5, 10));
    const [playerAtk, setPlayerAtk] = useState(getRndInteger(5, 10));

    const [playerSpcAtk, setPlayerSpcAtk] = useState(getRndInteger(10, 20));
    const [playerSpcAtkCharged, setPlayerSpcAtkCharged] = useState(2);

    const [monsterSpcAtk, setMonsterSpcAtk] = useState(getRndInteger(8, 16));
    const [monsterSpcAtkCharged, setMonsterSpcAtkCharged] = useState(0);

    const [heal, setHeal] = useState(getRndInteger(5, 15));

    const [playerButtonState, setPlayerButtonState] = useState(true);
    const [spcAtkButtonState, setSpcAtkButtonState] = useState(true);

    useEffect(() => {
        if (playerHP > 0 && monsterHP > 0) {
            if (playerTurn === false) {
                setTimeout(() => {attack()}, 1200);
                changeTurn();
            }
        }
    });

    function getRndInteger(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }

    const handlePlayerAtttack = () => {
        setPlayerAtk(getRndInteger(5, 10));
        console.log({ playerAtk })
        attack();
        changeTurn();
    }

    const handlePlayerSpecialAtttack = () => {
        setPlayerSpcAtk(getRndInteger(10, 20));
        console.log({ playerSpcAtk });
        playerSpecialAtk();
        changeTurn();
    }

    const handlePlayerHeal = () => {
        setHeal(getRndInteger(5, 15))
        console.log({ heal })
        setPlayerHP(playerHP + heal);
        changeTurn();
    }

    const handlePlayerGiveUp = () => {
        console.log("GGWP")
    }

    const attack = () => {
        if (playerTurn === true) {
            setMonsterHP(monsterHP - playerAtk);
            if (playerSpcAtkCharged < 2) {
                setPlayerSpcAtkCharged(playerSpcAtkCharged + 1)
            } else if (playerSpcAtkCharged === 2 && spcAtkButtonState !== true) {
                console.log("Special attack is ready to use!")
                setSpcAtkButtonState(true);
            }
        } else {
            if (monsterSpcAtkCharged === 3) {
                setMonsterSpcAtk(getRndInteger(8, 16));
                console.log({ monsterSpcAtk })
                setPlayerHP(playerHP - monsterSpcAtk);
                setMonsterSpcAtkCharged(0);
                changeStateButtons();
            } else {
                setMonsterAtk(getRndInteger(5, 10));
                console.log({ monsterAtk })
                setPlayerHP(playerHP - monsterAtk);
                setMonsterSpcAtkCharged(monsterSpcAtkCharged + 1);
                changeStateButtons();
            }
        }
    }

    const playerSpecialAtk = () => {
        if (playerTurn === true && playerSpcAtkCharged === 2) {
            setMonsterHP(monsterHP - playerSpcAtk);
            setPlayerSpcAtkCharged(0);
            setSpcAtkButtonState(false);
        }
    }

    const changeTurn = () => {
        if (playerHP > 0 && monsterHP > 0) {
            if (playerTurn === true) {
                setPlayerTurn(false)
                changeStateButtons();
            } else {
                setPlayerTurn(true)
            }
        } else {
            if (playerHP <= 0 ){
                console.log("You lose");
                setPlayerTurn(false);
                setPlayerButtonState(false);
                setSpcAtkButtonState(false);
            } else if (monsterHP <= 0) {
                console.log("You win")
                setPlayerTurn(false);
                setPlayerButtonState(false);
                setSpcAtkButtonState(false);
            }
        }
        //return playerTurn ? setPlayerTurn(false) : setPlayerTurn(true);
    }

    const changeStateButtons = () => {
        return playerButtonState ? setPlayerButtonState(false) : setPlayerButtonState(true);
    }

    return (
        <Container>
            <Table>
                <thead>
                    <tr>
                        <th>Jogador</th>
                        <th>Monstro</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><HealthBar className="Player" hpLeft={playerHP}/></td>
                        <td><HealthBar className="Monster" hpLeft={monsterHP}/></td>
                    </tr>
                </tbody>
            </Table>
            <ContainerButtons>
                <div>
                    <Button variant="primary" onClick={handlePlayerAtttack} disabled={!playerButtonState}>Ataque</Button>
                </div>
                <div>
                    <Button variant="dark" onClick={handlePlayerSpecialAtttack} disabled={!spcAtkButtonState}>Ataque Especial</Button>
                </div>
                <div>
                    <Button variant="success" onClick={handlePlayerHeal} disabled={playerHP >= 100 ? playerButtonState : !playerButtonState}>Curar</Button>
                </div>
                <div>
                    <Button variant="danger" onClick={handlePlayerGiveUp} disabled={!playerButtonState}>Desistir</Button>
                </div>
            </ContainerButtons>
        </Container>
    )
}
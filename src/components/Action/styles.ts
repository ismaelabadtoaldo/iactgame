import styled from "styled-components";
import 'bootstrap/dist/css/bootstrap.css';

export const Container = styled.div`
    
`;

export const ContainerButtons = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    gap: 2rem;
    margin-top: 2rem;

    div {
        background: var(--white);
        display:flex;
        align-items: center;
        justify-content: center;
    }

    button {
        font-size: 1.2rem;
        color: var(--white);
        border: 0;
        border-radius: 0.25rem;
        height: 3rem;
        width: 12rem;
        transition: filter 0.2s;
        
        &:hover {
            color: var(--white);
            filter: brightness(0.85);
        }
    }
`;
import { Container } from "./style";
import { ProgressBar } from "react-bootstrap";

export interface HealthBarProps {
    hpLeft: number;
    className: string;
}

export function HealthBar({ hpLeft, className } : HealthBarProps) {

    return (
        <Container>
            <ProgressBar className={className} variant="success" min={0} now={hpLeft} label={ hpLeft > 0 ? `${hpLeft}%` : 0} />
        </Container>
    );
}